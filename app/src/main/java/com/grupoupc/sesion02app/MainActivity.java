package com.grupoupc.sesion02app;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.grupoupc.sesion02app.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    //MainActivity = ActivityMainBinding

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //1. Evento Onclick del boton
        binding.btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               //2. Obtener los nombres y apellidos de las cajas de texto
               String nombres = binding.edtNombres.getText().toString();
               System.out.println(nombres);
               String apellidos = binding.edtApellidos.getText().toString();
               String resultadoConcatenacion = nombres + " " + apellidos;

               binding.tvResultado.setText(resultadoConcatenacion);
            }
        });
    }
}