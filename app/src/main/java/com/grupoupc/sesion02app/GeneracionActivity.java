package com.grupoupc.sesion02app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.grupoupc.sesion02app.databinding.ActivityGeneracionBinding;

public class GeneracionActivity extends AppCompatActivity {

    String resultadoGeneracion = "";

    //GeneracionActivity = ActivityGeneracionBinding
    ActivityGeneracionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityGeneracionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //<>
                int anioNacimiento =  Integer.parseInt(binding.edtNacimiento.getText().toString());

                if(anioNacimiento >= 1969 && anioNacimiento <= 1980){
                    resultadoGeneracion = "Generacion X";
                }
                else if(anioNacimiento >= 1981 && anioNacimiento <= 1993){
                    resultadoGeneracion = "Generacion Y";
                }
                else if(anioNacimiento >= 1994 && anioNacimiento <= 2010){
                    resultadoGeneracion = "Generacion Z";
                }
                else{
                    resultadoGeneracion = "No encontro su generacion";
                }

                binding.tvResultadoGeneracion.setText(resultadoGeneracion);

            }
        });
    }
}