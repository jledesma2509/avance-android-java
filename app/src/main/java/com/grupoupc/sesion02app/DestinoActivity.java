package com.grupoupc.sesion02app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.grupoupc.sesion02app.databinding.ActivityDestinoBinding;
import com.grupoupc.sesion02app.databinding.ActivityMainBinding;

public class DestinoActivity extends AppCompatActivity {

    ActivityDestinoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDestinoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle bundleRecepcion = getIntent().getExtras();
        String nombres = bundleRecepcion.getString("key_nombres","");
        String edad = bundleRecepcion.getString("key_edad","");
        String genero = bundleRecepcion.getString("key_genero","");

        binding.tvNombresDestino.setText(nombres);
        binding.tvEdadDestino.setText(edad);
        binding.tvGeneroDestino.setText(genero);
    }
}