package com.grupoupc.sesion02app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.grupoupc.sesion02app.databinding.ActivityMiRegistroBinding;

public class MiRegistroActivity extends AppCompatActivity {

    //MiRegistroActivity  ActivityMiRegistroBinding

    ActivityMiRegistroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMiRegistroBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //1. Recuperar los datos (inputs)
                String nombres = binding.edtNombresRegistro.getText().toString();
                String edad = binding.edtEdadRegistro.getText().toString();

                //2. Verificar que el nombre haya sido ingresado
                if(nombres.equals("")){

                    Toast.makeText(MiRegistroActivity.this,"Debe ingresar sus nombres",Toast.LENGTH_SHORT).show();
                    return;
                }

                //3. Verificar Edad
                if (edad.equals("")){

                    Toast.makeText(MiRegistroActivity.this,"Debe ingresar su edad",Toast.LENGTH_SHORT).show();
                    return;
                }

                //4. Los terminos y condiciones
                if (!binding.chkTerminos.isChecked()){

                    Toast.makeText(MiRegistroActivity.this,"Debe aceptar los terminos",Toast.LENGTH_SHORT).show();
                    return;
                }

                String genero;

                if (binding.rbMasculino.isChecked()){
                    genero = "Masculino";
                }
                else{
                    genero = "Femenino";
                }

                Bundle bundle = new Bundle();
                bundle.putString("key_nombres",nombres);
                bundle.putString("key_edad",edad);
                bundle.putString("key_genero",genero);


                Intent intent = new Intent(MiRegistroActivity.this,DestinoActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);




            }
        });

    }
}